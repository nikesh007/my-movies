import React from 'react'
import spinner from './spinner.gif'

function Spinner() {

  const style ={
      width: '200px',
      margin: 'auto',
      display: 'block'
  }
  return (
    <div>
      <img
        src={spinner}
        style={style}
        alt="Loading..."
      />
    </div>
  )
}

export default Spinner
