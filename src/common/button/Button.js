

import React from 'react'
import { Link } from 'react-router-dom';

const Button = (props) => {
    const { btnName, goTo } = props;
    return (
        <div>
            <Link to={goTo} >
                <button className="button">{btnName}</button>
            </Link>
        </div>
    )
}

export default Button;
