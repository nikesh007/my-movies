import React from 'react'
import './movieCard.scss'
import Button from '../../../common/button/Button';

const MovieCard = (props) => {
    const { movie } = props;
    return (
        <div className="cards_wrap">
            <div className="card_item">
                <div className="card_inner">
                    <div className="card_top">
                        <img src={movie.Poster} alt="car" />
                    </div>
                    <div className="card_bottom">
                        <div className="card_info">
                            <p className="title">{movie.Title}</p>
                            <p>{movie.Year}</p>
                        </div>
                    </div>
                    <Button btnName="Movie Details" goTo={'/movie/' + movie.imdbID} />
                </div>
            </div>
        </div>
    )
}

export default MovieCard;
