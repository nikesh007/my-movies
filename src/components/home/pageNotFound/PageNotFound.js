import React from 'react'
import { Link } from 'react-router-dom';

function PageNotFound() {
    return (
        <div>
            <h3>Page Not Found !</h3>
            <h3><Link to="/">Go to Home</Link></h3>
        </div>
    )
}

export default PageNotFound
